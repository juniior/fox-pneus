<?php get_header(); ?>

	<div id="main-content" class="interna container-full">
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="breadcrumb">
					  <li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
					  <li class="active"><?php the_title(); ?></li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="span9">
					<h3 class="titulo-fox"><?php the_title(); ?></h3>
				</div>
				<div class="span3">
					<h3 class="titulo-fox">Marcas</h3>
				</div>
			</div>
			<div class="row entry">
				<div class="span9"><?php 
				$paged = (get_query_var("paged")) ? get_query_var("paged") : 1;
				$_produtos = array('post_type' => 'cpt_produto', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 12, 'paged' => $paged);
				$produto = new WP_Query($_produtos);
				if ($produto->have_posts()) {
					$cont = 0;
					while ($produto->have_posts()) {
						$cont++; 
						if ($cont == 1) { ?>
							<div class="row entry"><?php									
						}
						$produto->the_post();
						$marcas = get_the_terms(get_the_ID(), 'tx_marca'); ?>						
						<div class="span3">
							<p class="text-branco"><i class="icon-tags icon-white"></i> <?php foreach ($marcas as $marca) { echo " ".$marca->name; } ?></p>
							<p class="img-polaroid img-rounded">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-275'); ?></a>
							</p>
							<h4 class="text-branco text-center"><?php the_title(); ?></h4>
							<h5 class="text-error text-center"><?php echo get_post_meta(get_the_ID(), 'preco', true); ?></h5>
						</div> <!-- fimSpan3 --> <?php
						if ($cont == 3) {
							$cont = 0; ?>
							</div> <!-- fimRow --> <div class="clearfix">&nbsp;</div> <?php
						}
					}
					if ($cont > 0 && $cont < 3) { ?>
						</div> <?php
					}
				} ?>
				<?php if(function_exists("pagination")) pagination($produto->max_num_pages); ?>
				</div> <!-- fimSpan9 -->
				<div class="span3 entry marcas">
					<?php $taxonomies = array("tx_marca"); 
						$args = array(
							'orderby' => 'name',
							'order' => 'ASC',
							'hierarchical' => 1
						); 
						$categorias = get_terms($taxonomies, $args); ?>
						<ul class="unstyled">						
						<?php foreach($categorias as $categoria): ?>
							<li><a class="text-branco" href="<?php echo get_term_link($categoria); ?>"><i class="icon-tags icon-white"></i>&nbsp;&nbsp;<?php echo $categoria->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="clearfix">&nbsp;</div>
	</div>

<?php get_footer(); ?>