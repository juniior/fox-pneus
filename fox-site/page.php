<?php get_header(); ?>


	<div id="main-content" class="interna container-full">
		<div class="container">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			<div class="row">
				<div class="span12">
					<ul class="breadcrumb">
					  	<li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
					  	<li class="active"><?php the_title(); ?></li>
					</ul>
				</div>
			</div>					
			<div class="row">
				<div class="span12">
					<h3 class="titulo-fox"><?php the_title(); ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="span12 entry">
					<?php //echo get_the_post_thumbnail(get_the_ID(),'img-220-300',array('class'=>'pull-left img-rounded img-polaroid','title'=>'')); ?>							
					<?php the_content(); ?>
				</div>
			</div>
			<?php endwhile; endif; ?>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="clearfix">&nbsp;</div>
	</div>


<?php get_footer(); ?>