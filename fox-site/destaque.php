<div id="slider" class="container">
	<div class="row">
		<div class="span12">
			<div id="myCarousel" class="carousel slide">
				<div class="carousel-inner"> <?php
					$_destaque = array('post_type' => 'cpt_destaque', 'orderby' => 'date', 'posts_per_page' => 5);
					$destaque = new WP_Query($_destaque);
					if ($destaque->have_posts()) { 
						$cont = 0;
						while ($destaque->have_posts()) {
							$destaque->the_post(); ?>
							<div class="item<?php if($cont == 0){ echo ' active'; } ?>">
								<a href="<?php echo get_post_meta(get_the_ID(),'link',true); ?>"><?php the_post_thumbnail('img-940-280', array('class' => 'img-rounded')); ?></a>
							</div> <?php
							$cont++;
						}
					} ?>
				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
			</div>
		</div>
	</div>
</div>