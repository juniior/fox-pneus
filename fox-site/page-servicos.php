<?php get_header(); ?>

	<div id="main-content" class="interna container-full">
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="breadcrumb">
					  <li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
					  <li class="active"><?php the_title(); ?></li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="span12">
					<h3 class="titulo-fox"><?php the_title(); ?></h3>
				</div>				
			</div>
			<div class="row entry">
				<div class="span12"><?php 
				$_servicos = array('post_type' => 'cpt_servico', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1);
				$servicos = new WP_Query($_servicos);
				if ($servicos->have_posts()) {
					$cont = 0;
					while ($servicos->have_posts()) {
						$cont++; 
						if ($cont == 1) { ?>
							<div class="row"><?php									
						}
						$servicos->the_post();?>						
						<div class="span6">
							<h4 class="text-branco"><?php the_title(); ?></h4>
							<div class="row">
								<div class="span3">
									<p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-275', array('class' => 'img-polaroid img-rounded')); ?></a></p>
								</div>
								<div class="span3">
									<p class="entry"><?php echo limite_resumo(150); ?></p>
								</div>
							</div>							
						</div> <?php
						if ($cont == 2) {
							$cont = 0; ?>
							</div> <?php
						}
					}
					if ($cont > 0 && $cont < 2) { ?>
						</div> <?php
					}
				} ?>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="clearfix">&nbsp;</div>
	</div>

<?php get_footer(); ?>