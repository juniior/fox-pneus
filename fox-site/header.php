<?php header("Connection: Keep-alive"); ?>
<?php header("X-UA-Compatible: IE=edge,chrome=1"); ?>
<?php header("Expires: 0"); ?>
<?php header("Cache-Control: max-age=0"); ?>
<?php header("Refresh: 600; url=http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>
<!doctype html>
<html lang="pt-BR">
	<head>
		<meta charset="UTF-8">
		<title><?php if (function_exists("is_tag") && is_tag()) { single_tag_title("Tag para &quot;"); echo "&quot; - "; } elseif (is_archive()) { echo "Categorias - "; wp_title(""); echo " - "; } elseif (is_search()) { echo "Busca por &quot;".wp_specialchars($s)."&quot; - "; } elseif (!(is_404()) && (is_single()) || (is_page())) { wp_title(""); echo " - "; } elseif (is_404()) { echo "Página não encontrada - "; } if (is_home()) { bloginfo("name"); echo " - "; bloginfo("description"); } else { bloginfo("name"); } if ($paged>1) { echo " - Página ".$paged; } ?></title>
        <meta name="description" content="<?php if (is_single()) { single_post_title('',true); } else { bloginfo('name'); echo " - "; bloginfo('description'); } ?>">
        <meta name="keywords" content="fox, pneus, rede, revenda, oficial, pirelli, bridgestone, firestone, cst, continental, yokohama, toyo">
		<meta name="author" content="Emiliano Matsumura [emilianomm@gmail.com] e Junior Santos [juniior.pvh@gmail.com]">
		<meta name="robots" content="index, follow">
		<!--link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/img/favicon.ico"-->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/plugin/fancybox/jquery.fancybox.css">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/main.css">
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<?php wp_head(); ?>
	</head>
	<body>
		<div id="site-wrapper" class="relative container-full">
			<header>
				<div class="container">
					<div class="row">
						<div class="span3">
							<a href="index.php"><img src="<?php bloginfo("template_url"); ?>/img/logo-header.png" alt="Fox Pneus"></a>
						</div>
						<div class="span9 relative">
							<img id="revenda-pirelli" class="absolute img-rounded" src="<?php bloginfo("template_url"); ?>/img/revenda-pirelli.png" alt="Revendedor Oficial Pirelli">				
							<?php wp_nav_menu(array('theme_location' => 'global', 'menu_id' => 'top-nav', 'depth' => 4, 'container' => false, 'menu_class' => 'unstyled relative pull-right', 'walker' => new twitter_bootstrap_nav_walker())); ?>
						</div>
					</div>
				</div>
			</header>