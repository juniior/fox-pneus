<?php get_header(); ?>


	<div id="main-content" class="interna container-full">
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="breadcrumb">
					  	<li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
					  	<li class="active">Página não encontrada</li>
					</ul>
				</div>
			</div>					
			<div class="row">
				<div class="span12">
					<h3 class="titulo-fox">Página não encontrada</h3>
				</div>
			</div>
			<div class="row">
				<div class="span12 entry">
					<p>Erro 404 - Página não encontrada.</p>
					<p>Utilize o menu acima para navegar no site ou <a href="<?php echo get_option("home"); ?>">clique aqui</a> para ir para o início do site.</p>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="clearfix">&nbsp;</div>
	</div>


<?php get_footer(); ?>