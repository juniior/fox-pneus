		<?php wp_footer(); ?>

		<footer class="container-full">
			<div class="container">
				<div class="row">
					<div class="span12 text-center">
						&copy; <?php echo date("Y"); ?> - <?php bloginfo("name"); ?> - Todos os Direitos Reservados
					</div>
				</div>
			</div>
		</footer>

		</div>

		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/jquery.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/jquery-easing.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/bootstrap.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/plugin/fancybox/jquery.fancybox.js"></script>
		<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/main.js"></script>
	</body>
</html>