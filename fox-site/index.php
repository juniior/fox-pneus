<?php get_header(); ?>

<?php get_template_part('destaque','destaque'); ?>

<div id="main-content" class="home container-full">
	<div class="container">
		<div class="row">
			<!-- Promoções -->
			<div class="span6">
				<h3 class="titulo-fox">Promoções
					&nbsp;&nbsp;&nbsp;
					<a id="a-promo" class="btn btn-small"><i class="icon-chevron-left"></i></a>
					&nbsp;
					<a id="p-promo" class="btn btn-small"><i class="icon-chevron-right"></i></a>
				</h3>
				<div id="promos">
					<div class="slider-wrapper relative"><?php  
						$_meta_query = array('key' => 'promo', 'value' => '1');
						$_promo = array('post_type' => 'cpt_produto', 'orderby' => 'rand', 'meta_query' => array($_meta_query), 'posts_per_page' => 18);
						$promo = new WP_Query($_promo);
						if ($promo->have_posts()) {
							$cont = 0;
							while ($promo->have_posts()) {
								$cont++;
								if ($cont == 1) { ?>
									<div class="row promo off absolute"> <?php									
								}
								$promo->the_post();
								$marcas = get_the_terms(get_the_ID(), 'tx_marca'); ?>
								<div class="span2">
									<p class="text-branco"><i class="icon-tags icon-white"></i> <?php foreach ($marcas as $marca) { echo " ".$marca->name; } ?></p>
									<p class="img-polaroid img-rounded">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-275'); ?></a>
									</p>
									<h4 class="text-branco text-center"><?php the_title(); ?></h4>
									<h5 class="text-error text-center"><?php echo get_post_meta(get_the_ID(), 'preco', true); ?></h5>
								</div> <?php
								if ($cont == 3) {
									$cont = 0; ?>
									</div> <?php
								}
							}
							if ($cont > 0 && $cont < 3) { ?>
								</div> <?php
							}
						} ?>
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>
				<div class="row"><?php
				$_banners = array('post_type' => 'cpt_banner', 'orderby' => 'rand', 'posts_per_page' => 2);
				$banner = new WP_Query($_banners);				
				if ($banner->have_posts()) { 
					while ($banner->have_posts()) {
						$banner->the_post(); ?>						
						<div class="span3"><a href="<?php echo get_post_meta(get_the_ID(),'link',true); ?>" target="_blank"><?php the_post_thumbnail('img-220-100', array('class' => 'img-polaroid img-rounded')); ?></a></div><?php 
					}
				} ?>
				</div>
			</div>
			<!-- Lançamento --> <?php  
			$_meta_query = array('key' => 'lancamento', 'value' => '1');
			$_lancamento = array('post_type' => 'cpt_produto', 'orderby' => 'rand', 'meta_query' => array($_meta_query), 'posts_per_page' => 1);
			$lancamento = new WP_Query($_lancamento);
			if ($lancamento->have_posts()) { ?>
				<div class="span3">
					<h3 class="titulo-fox">Lançamento</h3> <?php
					while ($lancamento->have_posts()) {
						$lancamento->the_post();
						$marcas = get_the_terms(get_the_ID(), 'tx_marca'); ?>
						<p class="text-branco"><i class="icon-tags icon-white"></i> <?php foreach ($marcas as $marca) { echo " ".$marca->name; } ?></p>
						<p class="img-polaroid img-rounded">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-275'); ?></a>
						</p>
						<h4 class="text-branco text-center"><?php the_title(); ?></h4>
						<h5 class="text-error text-center"><?php echo get_post_meta(get_the_ID(), 'preco', true); ?></h5> <?php
					} ?>
				</div> <?php
			} ?>
			<!-- Novidades --> <?php  			
			$_novidade = array('post_type' => 'post', 'orderby' => 'date', 'posts_per_page' => 2);
			$novidade = new WP_Query($_novidade);
			if ($novidade->have_posts()) { ?>
			<div class="span3">
				<h3 class="titulo-fox">Novidades</h3> <?php
					while ($novidade->have_posts()) {
						$novidade->the_post();?>
				<div>
					<p class="text-branco text-right"><small><?php the_time("d/m/Y"); ?></small></p>
					<p class="img-polaroid img-rounded"><?php the_post_thumbnail('img-220-210'); ?></p>
					<h5 class="text-branco"><?php the_title(); ?></h5>
					<a href="<?php the_permalink(); ?>" class="btn btn-small"><i class="icon-plus-sign"></i> Ler mais</a>
				</div>
				<div class="clearfix">&nbsp;</div><?php
					} ?>
			</div> <?php
			} ?>
		</div>
		<div class="row">
			<!-- Serviços -->
			<div class="span12">
				<h3 class="titulo-fox">Serviços</h3>
			</div> <?php 
			$_servicos = array('post_type' => 'cpt_servico', 'orderby' => 'rand', 'posts_per_page' => 3);
			$servico = new WP_Query($_servicos);
			if ($servico->have_posts()) { 
				while ($servico->have_posts()) {
					$servico->the_post(); ?>				
					<div class="span3"><p class="img-polaroid img-rounded"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-210'); ?></a></p></div><?php
				} 
			}?>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
</div>

<?php get_footer(); ?>