<?php
	
	include('core/menus.php');
	include('core/pagination.php');
	
	include('core/cpt-produto.php');
	include('core/cpt-banner.php');
	include('core/cpt-servicos.php');
	include('core/cpt-destaque.php');

	// Desabilita a edição de arquivos pela IDE do Wordpress
	@define('DISALLOW_FILE_EDIT', true);

	add_theme_support('post-thumbnails',array('post','cpt_produto','cpt_banner','cpt_servico','cpt_destaque'));

	// Remove os tamanhos padrões de imagem do wordpress. Gera economia de espaço e banda pois os mesmos não serão gerados apenas os personalizados + tamanho original.
	function sgr_filter_image_sizes($sizes) {
		unset($sizes['medium']);
		unset($sizes['large']);
		return $sizes;
	}
	add_filter('intermediate_image_sizes_advanced', 'sgr_filter_image_sizes');

	set_post_thumbnail_size(150, 9999, false);

	// Define tamanhos personalizados de imagens a serem geradas no upload 
	add_image_size('img-220-100',220,100,true);
	add_image_size('img-220-275',220,275,true);
	add_image_size('img-220-300',220,300,true);
	add_image_size('md',290,9999,true);
	add_image_size('gd',620,9999,true);
	add_image_size('img-940-280',940,280,true);

	// Define os tamanhos de imagens que deverão aparecer na tela de upload para inserção no post. Somente média!
	function custom_wmu_image_sizes($sizes) {
	    $sizes = array(
	        'md'=>__("Imagem Média"),
	        'gd'=>__("Imagem Grande")
	    );
	    return $sizes;
	}
	add_filter('image_size_names_choose', 'custom_wmu_image_sizes');

	// Adiciona as paginas, caso não exista
	verifica_pagina("agendamento","Agendamento");
	verifica_pagina("contato","Contato");
	verifica_pagina("produtos","Produtos");
	verifica_pagina("quem-somos","Quem Somos");
	verifica_pagina("servicos","Serviços");

	// Função que atualiza templates de páginas
	function verifica_pagina($_slug,$_nome) {
	  if(!get_page_by_title($_nome)) {
	    wp_insert_post(array(
	      "post_type" => "page",
	      "post_author" => 1,
	      "post_title" => $_nome,
	      "post_name" => $_slug,
	      "post_content" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	      "coment_status" => "closed",
	      "post_status" => "publish"
	    ));
	  }
	}

	// Limita os caracteres do resumo nativo do wordpress
	function limite_resumo($charlength) {
	  $excerpt = get_the_excerpt();
	  $charlength++;
	  if (mb_strlen($excerpt) > $charlength) {
	    $subex = mb_substr($excerpt, 0, $charlength - 5);
	    $exwords = explode(' ', $subex);
	    $excut = - (mb_strlen($exwords[ count( $exwords ) - 1 ]));
	    if ( $excut < 0 ) {
	      $excerpt = mb_substr($subex, 0, $excut);
	    } else {
	      $excerpt = $subex;
	    }
	  }
	  return $excerpt;
	}