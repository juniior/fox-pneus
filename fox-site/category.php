<?php get_header(); ?>

	<?php $paged = (get_query_var("paged")) ? get_query_var("paged") : 1; ?>
    <?php $posts = array("order_by"=>"date","order"=>"desc","posts_per_page"=>5,"paged"=>$paged); ?>
    <?php $posts = array_merge($posts, $wp_query->query); ?>
    <?php query_posts($posts); ?>
	<div id="main-content" class="interna container-full">
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="breadcrumb">
					  <li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
					  <li class="active"><?php wp_title(); ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="span9">
					<h3 class="titulo-fox"><?php wp_title(); ?></h3>
				</div>
				<div class="span3">
					<h3 class="titulo-fox">Categorias</h3>
				</div>
			</div>
			<div class="row entry">
				<div class="span9">
                    <?php if(have_posts()): while(have_posts()): the_post(); ?>
                        <?php $img = get_the_post_thumbnail(get_the_ID(),'img-220-300',array('class'=>'pull-left img-rounded img-polaroid','title'=>'')); ?>
                        <?php if($img == '') { ?>
                            <div class="row">
                                <div class="span9">
                                    <h4><a href="<?php the_permalink(); ?>" class="text-amarelo"><?php the_title(); ?></a></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span9 entry">
                                    <small><?php the_time('d \d\e F \d\e Y'); ?></small>
                                    <br>
                                    <a href="<?php the_permalink(); ?>" class="text-amarelo"><?php echo limite_resumo(220);  ?></a>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="span9">
                                    <h4><a href="<?php the_permalink(); ?>" class="text-branco"><?php the_title(); ?></a></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span9 entry">
                                    <?php echo $img; ?>
                                    <small class="text-amarelo"><?php the_time('d \d\e F \d\e Y'); ?></small>
                                    <br>
                                    <p><a href="<?php the_permalink(); ?>" class="text-branco"><?php echo limite_resumo(220);  ?></a></p>
                                </div>
                            </div>
                        <?php } ?>
                        <hr>
                    <?php endwhile; ?>
                    <?php else: ?>
                        <div class="alert alert-warning fade in">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            Nenhuma <b>item</b> encontrada nesta categoria.
                        </div>
                    <?php endif; ?>
				</div>
				<div class="span3 entry marcas"> <?php 
					$args = array(
                            'type' => 'post',
                            'orderby' => 'name',
                            'order' => 'ASC',
                            'hide_empty' => 1,
                            'hierarchical' => 1,
                            'taxonomy' => 'category'
                        ); 
                    $categorias = get_categories($args); ?>
					<ul class="unstyled">
 						<!-- <li><a class="text-branco" href="<?php echo get_option('home'); ?>/todas-noticias/"><i class="icon-tags icon-white"></i>&nbsp;&nbsp;Todas</a></li> -->
                    <?php foreach($categorias as $categoria): ?>
                        <li><a class="text-branco" href="<?php echo get_category_link($categoria->term_id); ?>"><i class="icon-tags icon-white"></i>&nbsp;&nbsp;<?php echo $categoria->name; ?></a></li>
                    <?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="clearfix">&nbsp;</div>
	</div>

<?php get_footer(); ?>