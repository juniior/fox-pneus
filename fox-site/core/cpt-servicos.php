<?php 
	add_action('init','create_cpt_servico'); // Inicializa a função que criar o tipo de post
	// Função que cria o tipo de post
	function create_cpt_servico() {
	    //Registra tipo de post personalizado
	    register_post_type(
	        // Nome (slug) do tipo de post. Padrão: 'cpt_' como prefixo do nome do tipo de post
	        'cpt_servico',
	        array(
	            // Nomes do tipo de post
	            'labels' => array(
	                'name' => 'Serviço',
	                'singular_name' => 'Serviço',
	                'add_new' => 'Adicionar Novo',
	                'all_items' => 'Todos os Serviços',
	                'add_new_item' => 'Adicionar Novo Serviço',
	                'edit' => 'Alterar',
	                'edit_item' => 'Alterar Serviço',
	                'new_item' => 'Novo Serviço',
	                'view' => 'Visualizar',
	                'view_item' => 'Visualizar Serviço',
	                'search_items' => 'Buscar Serviços',
	                'not_found' => 'Nenhum Serviço encontrado',
	                'not_found_in_trash' => 'Nenhum Serviço encontrado na lixeira',
	                'parent' => 'Serviço Pai'
	            ),
	            // Descrição do tipo de post
	            'description' => 'Serviço para exibição. O produto deverá ser cadastrada com Título (Nome), descrição e Marca (Pirelli, Continental ou Outros).',
	            // Define se o tipo de post é público ou não - tanto para o back-end quanto para o front-end
	            'public' => true,
	            // Define se o tipo de post estará disponível nos resultados de busca padrão do wordpress
	            'exclude_from_search' => false,
	            // Define se o tipo de post poderá ser utilizado em consultas no front-end (Exemplo: post_type={post_type_slug})
	            'publicly_queryable' => true,
	            // Define se o tipo de post terá uma interface no painel, ou não
	            'show_ui' => true,
	            // Define se um tipo de post estará ou não disponível para menus de navegação
	            'show_in_nav_menus' => false,
	            // Define se um tipo de post estará ou não disponível no menu do back-end
	            'show_in_menu' => true,
	            // Define se um tipo de post estará ou não disponível na barra de administração do front-end quando um usuário estiver navegando enquanto logado no back-end
	            'show_in_admin_bar' => true,
	            // Define a posição do tipo do post no menu do back-end
	            'menu_position' => 5,
	            // Defien um ícone personalizado para o tipo de post
	            'menu_icon' => null,
	            // Define o tipo de capacidades associadas ao tipo de post
	            'capability_type' => 'page',
	            // Define especificamenet as capacidades do tipo de post. O atributo 'capability_type' é utilizado como construtor por padrão
	            // 'capabilities' => 
	            // Define a utilização das capacidades padrões de meta-informações
	            // 'map_meta_cap' => true,
	            // Define se o tipo de post é hierárquico ou não. A opção 'page-attributes' deve estar adicionada no atributo 'supports' para funcionar
	            'hierarchical' => false,
	            // Define quais campos/funcionalidades estarão disponíveis no tipo de post. Opções: title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats
	            'supports' => array(
	                'title',
					'editor',
					'thumbnail'
	            ),
	            // Define uma função que será chamada ao montar a interface de edição do tipo de post
	            //'register_meta_box_cb' => 'nome_da_funcao',
	            // Define um tipo personalizado de taxonomia (categoria) para o tipo de post
	            //'taxonomies' => array('tx_marca'),
	            // Define que o tipo de post terá arquivo. Padrão é 'false'
	            //'has_archive' => true,
	            // Define como ficará a slug do tipo de post quando utilizar URL Amigável
	            'rewrite' => array(
	                'slug'=>'servico'
	            ),
	            // Define se e como o tipo de post será consultado no front-end
	            //'query_var' => 'nome_personalizado',
	            // Define se este tipo de post poderá ou não ser exportado no back-end. Padrão: true
	            //'can_export' => false
	        )
	    );
	}

 ?>