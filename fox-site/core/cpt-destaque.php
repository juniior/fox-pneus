<?php 

	add_action('init','create_cpt_destaque'); // Inicializa a função que criar o tipo de post
	// Função que cria o tipo de post
	function create_cpt_destaque() {
	  //Registra tipo de post personalizado
	  register_post_type(
	    // Nome (slug) do tipo de post. Padrão: 'cpt_' como prefixo do nome do tipo de post
	    'cpt_destaque',
	    array(
	      // Nomes do tipo de post
	      'labels' => array(
	        'name' => 'Destaques',
	        'singular_name' => 'Destaque',
	        'add_new' => 'Adicionar Novo',
	        'all_items' => 'Todos os Destaques',
	        'add_new_item' => 'Adicionar Novo Destaque',
	        'edit' => 'Alterar',
	        'edit_item' => 'Alterar Destaque',
	        'new_item' => 'Novo Destaque',
	        'view' => 'Visualizar',
	        'view_item' => 'Visualizar Destaque',
	        'search_items' => 'Buscar Destaques',
	        'not_found' => 'Nenhum Destaque encontrado',
	        'not_found_in_trash' => 'Nenhum Destaque encontrado na lixeira',
	        'parent' => 'Destaque Pai'
	      ),
	      // Descrição do tipo de post
	      'description' => 'Destaques de divulgação da entidade. Os destaques de divulgação da entidade deverão ser cadastrados com Título (para fim de organização - o mesmo não aparecerá no front-end), Link e Imagem de Capa.',
	      // Define se o tipo de post é público ou não - tanto para o back-end quanto para o front-end
	      'public' => true,
	      // Define se o tipo de post estará disponível nos resultados de busca padrão do wordpress
	      'exclude_from_search' => true,
	      // Define se o tipo de post poderá ser utilizado em consultas no front-end (Exemplo: post_type={post_type_slug})
	      'publicly_queryable' => true,
	      // Define se o tipo de post terá uma interface no painel, ou não
	      'show_ui' => true,
	      // Define se um tipo de post estará ou não disponível para menus de navegação
	      'show_in_nav_menus' => false,
	      // Define se um tipo de post estará ou não disponível no menu do back-end
	      'show_in_menu' => true,
	      // Define se um tipo de post estará ou não disponível na barra de administração do front-end quando um usuário estiver navegando enquanto logado no back-end
	      'show_in_admin_bar' => true,
	      // Define a posição do tipo do post no menu do back-end
	      'menu_position' => 5,
	      // Defien um ícone personalizado para o tipo de post
	      'menu_icon' => null,
	      // Define o tipo de capacidades associadas ao tipo de post
	      'capability_type' => 'page',
	      // Define especificamenet as capacidades do tipo de post. O atributo 'capability_type' é utilizado como construtor por padrão
	      // 'capabilities' =>
	      // Define a utilização das capacidades padrões de meta-informações
	      // 'map_meta_cap' => true,
	      // Define se o tipo de post é hierárquico ou não. A opção 'page-attributes' deve estar adicionada no atributo 'supports' para funcionar
	      'hierarchical' => false,
	      // Define quais campos/funcionalidades estarão disponíveis no tipo de post. Opções: title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats
	      'supports' => array(
	        'title',
	        'thumbnail'
	      ),
	      // Define uma função que será chamada ao montar a interface de edição do tipo de post
	      //'register_meta_box_cb' => 'nome_da_funcao',
	      // Define um tipo personalizado de taxonomia (categoria) para o tipo de post
	      //'taxonomies' => array('post_tag','taxonomia_personalizada'),
	      // Define que o tipo de post terá arquivo. Padrão é 'false'
	      //'has_archive' => true,
	      // Define como ficará a slug do tipo de post quando utilizar URL Amigável
	      'rewrite' => array(
	        'slug'=>'destaque'
	      ),
	      // Define se e como o tipo de post será consultado no front-end
	      //'query_var' => 'nome_personalizado',
	      // Define se este tipo de post poderá ou não ser exportado no back-end. Padrão: true
	      //'can_export' => false
	    )
	  );
	}
	
	// Link
	add_action('admin_init','ui_link_destaque');
	function ui_link_destaque() {
	    add_meta_box('cf_link_destaque', 'Link', 'cf_link_destaque', 'cpt_destaque', 'normal', 'high');
	}
	function cf_link_destaque($post) {
	    $link = esc_html(get_post_meta($post->ID, 'link', true)); ?>
	    <table style="width:100%">
	        <tr><td><input type="text" name="link" style="width:100%" <?php if (!empty($link)) { echo 'value="'.$link.'"'; } ?> /></td></tr>
	    </table> <?php
	}

	//Salvando Campos personalizados
	add_action('save_post','save_ui_destaque',10,2);
	function save_ui_destaque($post_ID,$post) {
	  if($post->post_type == 'cpt_destaque') {
	    if(isset($_POST['link'])) {
	      update_post_meta($post_ID,'link',$_POST['link']);
	    }	    
	  }
	}

	add_filter('manage_edit-cpt_destaque_columns','cols_destaque'); // Registra um função que é disparada quando a lista de posts deste tipo de post é carregada
	// Função que adiciona colunas e ordaneção extra a listagem deste tipo de post
	function cols_destaque($cols) {
	  $cols['link'] = 'Link';
	  unset($cols['comments']);
	  return $cols;
	}

	add_action('manage_posts_custom_column','populate_cols_destaque'); // Registra um função para popular as colunas
	// Função que irá popular as colunas
	function populate_cols_destaque($col) { 
	  if('link' == $col) {
	    $link = esc_html(get_post_meta(get_the_ID(),'link',true));
	    echo $link;
	  }	  
	}

	add_filter('manage_edit-cpt_produto_sortable_columns','sort_cf_destaque'); // Registra função que dispara quando WP identifica uma coluna ordenável
	function sort_cf_destaque($cols) {	 
	  $cols['link'] = 'link';
	  return $cols;
	}

	add_filter('request','destaque_orderby'); // Função que identifica a coluna e torna ordenável
	function destaque_orderby($vars) {
	  if(!is_admin())
	    return $vars;
	  if(isset($vars['orderby']) && ('link' == $vars['orderby'])) {
	    $vars = array_merge($vars,array(
	      'meta_key' => 'link',
	      'orderby' => 'meta_value'
	    ));
	  }	  
	  
	  return $vars;
	}

 ?>