<?php 

	add_action('init','create_cpt_produto'); // Inicializa a função que criar o tipo de post
	// Função que cria o tipo de post
	function create_cpt_produto() {
	    //Registra tipo de post personalizado
	    register_post_type(
	        // Nome (slug) do tipo de post. Padrão: 'cpt_' como prefixo do nome do tipo de post
	        'cpt_produto',
	        array(
	            // Nomes do tipo de post
	            'labels' => array(
	                'name' => 'Produto',
	                'singular_name' => 'Produto',
	                'add_new' => 'Adicionar Novo',
	                'all_items' => 'Todos os Produtos',
	                'add_new_item' => 'Adicionar Novo Produto',
	                'edit' => 'Alterar',
	                'edit_item' => 'Alterar Produto',
	                'new_item' => 'Novo Produto',
	                'view' => 'Visualizar',
	                'view_item' => 'Visualizar Produto',
	                'search_items' => 'Buscar Produtos',
	                'not_found' => 'Nenhum Produto encontrado',
	                'not_found_in_trash' => 'Nenhum Produto encontrado na lixeira',
	                'parent' => 'Produto Pai'
	            ),
	            // Descrição do tipo de post
	            'description' => 'Produto para exibição. O produto deverá ser cadastrada com Título (Nome), descrição e Marca (Pirelli, Continental ou Outros).',
	            // Define se o tipo de post é público ou não - tanto para o back-end quanto para o front-end
	            'public' => true,
	            // Define se o tipo de post estará disponível nos resultados de busca padrão do wordpress
	            'exclude_from_search' => false,
	            // Define se o tipo de post poderá ser utilizado em consultas no front-end (Exemplo: post_type={post_type_slug})
	            'publicly_queryable' => true,
	            // Define se o tipo de post terá uma interface no painel, ou não
	            'show_ui' => true,
	            // Define se um tipo de post estará ou não disponível para menus de navegação
	            'show_in_nav_menus' => false,
	            // Define se um tipo de post estará ou não disponível no menu do back-end
	            'show_in_menu' => true,
	            // Define se um tipo de post estará ou não disponível na barra de administração do front-end quando um usuário estiver navegando enquanto logado no back-end
	            'show_in_admin_bar' => true,
	            // Define a posição do tipo do post no menu do back-end
	            'menu_position' => 5,
	            // Defien um ícone personalizado para o tipo de post
	            'menu_icon' => null,
	            // Define o tipo de capacidades associadas ao tipo de post
	            'capability_type' => 'page',
	            // Define especificamenet as capacidades do tipo de post. O atributo 'capability_type' é utilizado como construtor por padrão
	            // 'capabilities' => 
	            // Define a utilização das capacidades padrões de meta-informações
	            // 'map_meta_cap' => true,
	            // Define se o tipo de post é hierárquico ou não. A opção 'page-attributes' deve estar adicionada no atributo 'supports' para funcionar
	            'hierarchical' => false,
	            // Define quais campos/funcionalidades estarão disponíveis no tipo de post. Opções: title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats
	            'supports' => array(
	                'title',
					'editor',
					'thumbnail'
	            ),
	            // Define uma função que será chamada ao montar a interface de edição do tipo de post
	            //'register_meta_box_cb' => 'nome_da_funcao',
	            // Define um tipo personalizado de taxonomia (categoria) para o tipo de post
	            'taxonomies' => array('tx_marca'),
	            // Define que o tipo de post terá arquivo. Padrão é 'false'
	            //'has_archive' => true,
	            // Define como ficará a slug do tipo de post quando utilizar URL Amigável
	            'rewrite' => array(
	                'slug'=>'marca'
	            ),
	            // Define se e como o tipo de post será consultado no front-end
	            //'query_var' => 'nome_personalizado',
	            // Define se este tipo de post poderá ou não ser exportado no back-end. Padrão: true
	            //'can_export' => false
	        )
	    );
	}

	add_action('init','produto_taxonomy',0); // Registra uma função que cria uma taxonomia para o tipo de post
	// Função que cria uma taxonomia para o tipo de post
	function produto_taxonomy() {
	    register_taxonomy(
	        // Nome (slug) da taxonomia
	        'tx_marca',
	        // Tipo de post ao qual a taxonomia pertence
	        'cpt_produto',
	        array(
	            // Nomes da taxonomia
	            array(
	            	'name' => 'Marca',
	            	'singular_name' => 'Marca',
	            	'add_new' => 'Adicionar Novo',
	            	'all_items' => 'Todas as Marcas',
	            	'add_new_item' => 'Adicionar Nova Marca',
	            	'edit' => 'Alterar',
	            	'edit_item' => 'Alterar Marca',
	            	'new_item' => 'Novo Marca',
	            	'view' => 'Visualizar',
	            	'view_item' => 'Visualizar Marca'
	            ),
	            // Define se a taxonomia é pública ou não
	            'public' => true,
	            // Define se a taxonomia terá uma interface no back-end
	            'show_ui' => true,
	            // Define se a taxonomia aparecerá ou não para o plugin de tag clouds
	            'show_tagcloud' => false,
	            // Define se a taxonomia aparecerá como uma coluna em posts associados
	            'show_admin_column' => true,
	            // Define se as categorias poderão ou não ter descendentes
	            'hierarchical' => true,
	            'rewrite' => array(
	                'slug' => 'marca'
	            )
	        )
	    );
	}

	// Preço
	add_action('admin_init','ui_preco_produto');
	function ui_preco_produto() {
	    add_meta_box('cf_preco_produto', 'Preço', 'cf_preco_produto', 'cpt_produto', 'normal', 'high');
	}
	function cf_preco_produto($post) {
	    $preco = esc_html(get_post_meta($post->ID, 'preco', true)); ?>
	    <table style="width:100%">
	        <tr><td><input type="text" name="preco" style="width:100%" <?php if (!empty($preco)) { echo 'value="'.$preco.'"'; } ?> /></td></tr>
	    </table> <?php
	}

	// Promoção
	add_action('admin_init','ui_promo_produto');
	function ui_promo_produto() {
	    add_meta_box('cf_promo_produto', 'Promoção', 'cf_promo_produto', 'cpt_produto', 'normal', 'high');
	}
	function cf_promo_produto($post) {
	    $promo = esc_html(get_post_meta($post->ID, 'promo', true)); ?>
	    <table style="width:100%">
	        <tr><td>
	        	<select name="promo" style="width:100%">
	        		<option value="0">Não</option>
	        		<?php if (isset($promo) && $promo == 1): ?>
						<option value="1" selected="selected">Sim</option>
	        		<?php else: ?>
	        			<option value="1">Sim</option>
	        		<?php endif ?>
	        	</select>
	        </td></tr>
	    </table> <?php
	}

	// Lançamento
	add_action('admin_init','ui_lancamento_produto');
	function ui_lancamento_produto() {
	    add_meta_box('cf_lancamento_produto', 'Lançamento', 'cf_lancamento_produto', 'cpt_produto', 'normal', 'high');
	}
	function cf_lancamento_produto($post) {
	    $lancamento = esc_html(get_post_meta($post->ID, 'lancamento', true)); ?>
	    <table style="width:100%">
	        <tr><td>
	        	<select name="lancamento" style="width:100%">
	        		<option value="0">Não</option>
	        		<?php if (isset($lancamento) && $lancamento == 1): ?>
						<option value="1" selected="selected">Sim</option>
	        		<?php else: ?>
	        			<option value="1">Sim</option>
	        		<?php endif ?>
	        	</select>
	        </td></tr>
	    </table> <?php
	}

	//Salvando Campos personalizados
	add_action('save_post','save_ui_produto',10,2);
	function save_ui_produto($post_ID,$post) {
	  if($post->post_type == 'cpt_produto') {
	    if(isset($_POST['preco'])) {
	      update_post_meta($post_ID,'preco',$_POST['preco']);
	    }
	    if(isset($_POST['promo'])) {
	      update_post_meta($post_ID,'promo',$_POST['promo']);
	    }
	    if(isset($_POST['lancamento'])) {
	      update_post_meta($post_ID,'lancamento',$_POST['lancamento']);
	    }
	  }
	}

	add_filter('manage_edit-cpt_produto_columns','cols_produto'); // Registra um função que é disparada quando a lista de posts deste tipo de post é carregada
	// Função que adiciona colunas e ordaneção extra a listagem deste tipo de post
	function cols_produto($cols) {
	  $cols['lancamento'] = 'Lançamento';
	  $cols['promo'] = 'Promoção';
	  $cols['preco'] = 'Preço';
	  unset($cols['comments']);
	  return $cols;
	}

	add_action('manage_posts_custom_column','populate_cols_produto'); // Registra um função para popular as colunas
	// Função que irá popular as colunas
	function populate_cols_produto($col) { 
	  if('lancamento' == $col) {
	    $lancamento = esc_html(get_post_meta(get_the_ID(),'lancamento',true));
	    echo $lancamento;
	  }
	  if('promo' == $col) {
	    $promo = esc_html(get_post_meta(get_the_ID(),'promo',true));
	    echo $promo;
	  }
	  if('preco' == $col) {
	    $preco = esc_html(get_post_meta(get_the_ID(),'preco',true));
	    echo $preco;
	  }
	}

	add_filter('manage_edit-cpt_produto_sortable_columns','sort_cf_produto'); // Registra função que dispara quando WP identifica uma coluna ordenável
	function sort_cf_produto($cols) {
	  $cols['preco'] = 'preco';
	  $cols['promo'] = 'promo';
	  $cols['lancamento'] = 'lancamento';
	  return $cols;
	}

	add_filter('request','produto_orderby'); // Função que identifica a coluna e torna ordenável
	function produto_orderby($vars) {
	  if(!is_admin())
	    return $vars;
	  if(isset($vars['orderby']) && ('lancamento' == $vars['orderby'])) {
	    $vars = array_merge($vars,array(
	      'meta_key' => 'lancamento',
	      'orderby' => 'meta_value'
	    ));
	  }
	  if(isset($vars['orderby']) && ('promo' == $vars['orderby'])) {
	    $vars = array_merge($vars,array(
	      'meta_key' => 'promo',
	      'orderby' => 'meta_value'
	    ));
	  }
	  if(isset($vars['orderby']) && ('preco' == $vars['orderby'])) {
	    $vars = array_merge($vars,array(
	      'meta_key' => 'preco',
	      'orderby' => 'meta_value'
	    ));
	  }
	  return $vars;
	}

 ?>