<?php get_header(); ?>	

<div id="main-content" class="interna container-full">
	<div class="container">
		
		<div class="row">
			<div class="span12">

				<ul class="breadcrumb">
				  <li><a href="index.php">Fox Pneus</a> <span class="divider">/</span></li>
			  <?php $pagina = new WP_Query(array("pagename" => "produtos")); 
			  		while($pagina->have_posts()): $pagina->the_post();
					$_id = get_the_ID(); ?>
				  <li><a href="<?php echo get_page_link($_id); ?>">Produtos</a> <span class="divider">/</span></li>
				  <?php endwhile; ?>
				  <?php if(have_posts()): while(have_posts()): the_post(); ?>
				  <li class="active"><?php the_title(); ?></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="span8">
				<h3 class="titulo-fox"><?php the_title(); ?></h3>
			</div>
			<div class="span4">
				<h3 class="titulo-fox">Mais Produtos</h3>
			</div>
		</div>
		<div class="row entry">
			<div class="span3">				
				<?php echo get_the_post_thumbnail(get_the_ID(),'img-220-275',array('class'=>'img-rounded img-polaroid','title'=>'')); ?>
			</div>
			<div class="span5"><?php
				$marcas = get_the_terms(get_the_ID(), 'tx_marca'); ?>
				<p class="text-branco"><i class="icon-tags icon-white"></i> <?php foreach ($marcas as $marca) { echo " ".$marca->name; } ?></p>
				<p><?php the_content(); ?></p>
				<h2 class="text-error"><?php echo get_post_meta(get_the_ID(), 'preco', true); ?></h2>
			</div>
			<div class="span4">
				<div class="row"><?php 
					$_produtos = array("post_type"=>"cpt_produto","order"=>"ASC","orderby"=>"rand","posts_per_page"=>2);
					$produto = new WP_Query($_produtos);
					while($produto->have_posts()): $produto->the_post(); 
					$marcas = get_the_terms(get_the_ID(), 'tx_marca'); ?>
					<div class="span2">
						<p class="text-branco"><i class="icon-tags icon-white"></i> <?php foreach ($marcas as $marca) { echo " ".$marca->name; } ?></p>
						<p class="img-polaroid img-rounded">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-220-275'); ?></a>
						</p>
						<h4 class="text-branco text-center"><?php the_title(); ?></h4>
						<h5 class="text-error text-center"><?php echo get_post_meta(get_the_ID(), 'preco', true); ?></h5>
					</div>
				<?php endwhile; ?>					
				</div>
			</div>
		</div>
		<?php endwhile; endif; ?>
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="clearfix">&nbsp;</div>
</div>


<?php get_footer(); ?>