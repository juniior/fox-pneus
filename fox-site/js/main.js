/*!
 * jQuery Cookie Plugin v1.3
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
 (function(f,b,g){var a=/\+/g;function e(h){return h}function c(h){return decodeURIComponent(h.replace(a," "))}var d=f.cookie=function(p,o,u){if(o!==g){u=f.extend({},d.defaults,u);if(o===null){u.expires=-1}if(typeof u.expires==="number"){var q=u.expires,s=u.expires=new Date();s.setDate(s.getDate()+q)}o=d.json?JSON.stringify(o):String(o);return(b.cookie=[encodeURIComponent(p),"=",d.raw?o:encodeURIComponent(o),u.expires?"; expires="+u.expires.toUTCString():"",u.path?"; path="+u.path:"",u.domain?"; domain="+u.domain:"",u.secure?"; secure":""].join(""))}var h=d.raw?e:c;var r=b.cookie.split("; ");for(var n=0,k=r.length;n<k;n++){var m=r[n].split("=");if(h(m.shift())===p){var j=h(m.join("="));return d.json?JSON.parse(j):j}}return null};d.defaults={};f.removeCookie=function(i,h){if(f.cookie(i)!==null){f.cookie(i,null,h);return true}return false}})(jQuery,document);

// NOTICE!! DO NOT USE ANY OF THIS JAVASCRIPT
// IT'S ALL JUST JUNK FOR OUR DOCS!
// ++++++++++++++++++++++++++++++++++++++++++

!function ($) {

  $(function() {

    /**
     *  Slider Planos - Home
     **/
    $('.promo.off:first').addClass('on').removeClass('off');
    $('.promo.off').animate({ opacity: 0 }, 0);
    $('#p-promo').click(function(){
      var thisSlide = $('.promo.on');
      if ($('.promo.on').next('.promo.off').size() > 0) {
        var nextSlide = $('.promo.on').next('.promo.off');
      } else {
        var nextSlide = $('.promo.off:first');
      }
      thisSlide.animate({ left: '-100px', opacity: 0 }, 'slow', 'easeOutBack').removeClass('on').addClass('off');
      nextSlide.css('left','100px').animate({ left: '0px', opacity: 1 }, 'slow', 'easeOutBack').removeClass('off').addClass('on');
    });
    $('#a-promo').click(function(){
      var thisSlide = $('.promo.on');
      if ($('.promo.on').prev('.promo.off').size() > 0) {
        var nextSlide = $('.promo.on').prev('.promo.off');
      } else {
        var nextSlide = $('.promo.off:last');
      }
      thisSlide.animate({ left: '100px', opacity: 0 }, 'slow', 'easeOutBack').removeClass('on').addClass('off');
      nextSlide.css('left','-100px').animate({ left: '0px', opacity: 1 }, 'slow', 'easeOutBack').removeClass('off').addClass('on');
    });

  })

}(window.jQuery)